import { writable } from 'svelte/store';

const defupdate = {
  refreshed: 0,
  success: false,
  // online: true,
  counter: 0,
  event: null,
  current: null,
};

export const updateStore = writable(defupdate, async function start(set) {
  set(defupdate);
  // console.log($updateStore);
});
// export const updateStore = writable(defupdate);
