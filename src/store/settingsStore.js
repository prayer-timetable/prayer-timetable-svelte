import { readable } from 'svelte/store';
// import { formatDistance } from 'date-fns';

import defsettings from '../config/settings.json';
import { remote, local } from './actions/fetch'; // , local
import checkIfNeeded from './actions/checkIfNeeded';
import { updateStore } from './updateStore';

let update;
const unsubscribeUpdate = updateStore.subscribe(value => {
  update = value;
});

// first argument for readable is default - initial value
export const settingsStore = readable(defsettings, async function start(set) {
  // console.log('starting fetch local')
  let localResult;
  try {
    localResult = await local();
  } catch (e) {
    console.log(e);
  }

  set(localResult || defsettings); // in case all fails

  // console.log('starting fetch remote')
  const remoteResult = await remote();
  if (remoteResult) set(remoteResult);

  // define what should be done in tick
  const tick = async () => {
    // console.log("ticked settings");
    const needed = checkIfNeeded(update.refreshed, update.success, defsettings.updateInterval);

    const result = needed ? await remote() : null;

    // console.log('needed:', needed, 'result:', result);
    if (result) set(result);
  };
  // tick once
  tick();

  // define interval
  const interval = setInterval(tick, 1000 * 60);

  return function stop() {
    clearInterval(interval);
    unsubscribeUpdate();
  };
});
