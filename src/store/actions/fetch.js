// @ts-ignore
import axios from 'axios';
import localforage from 'localforage';

import { format, formatDistance } from 'date-fns';

// @ts-ignore

// @ts-ignore
import settings from '../../config/settings.json'; // ,online

import { updateStore } from '../updateStore';

const { log, url } = settings;

const API_URI = url.settings;
// import { settings } from '../settings'

/**
 * **********
 * ONLINE
 * **********
 */

const remote = async () => {
  // console.log('run online')
  let result = null;

  // console.log(
  //   needed,
  //   'refreshed:',
  //   update.refreshed,
  //   'success:',
  //   update.success,
  //   'updateInterval:',
  //   settings.updateInterval
  // )

  // update.subscribe(value => {
  //   console.log('update value', value)
  // })
  // console.log('result', result)

  try {
    const { data: newsettings } = await axios.get(API_URI, {
      method: 'GET',
      // mode: 'cors',
      // crossdomain: true,
      // withCredentials: true,
    });

    result = { ...newsettings };
    result.join === '0' && (result.join = false);

    updateStore.update(n => {
      n.success = true;
      return n;
    });
    updateStore.update(n => {
      n.refreshed = new Date();
      return n;
    });
    updateStore.update(n => {
      n.refreshedDiff =
        n.refreshed !== '-' && n.refreshed > 0
          ? `${formatDistance(new Date(n.refreshed), new Date(), {
              includeSeconds: true,
            })} ago
  `
          : '-';
      return n;
    });
    updateStore.update(n => {
      n.counter++;
      return n;
    });
    // console.log(result)

    await localforage.setItem('settings', result);
    await localforage.setItem('refreshed', new Date().valueOf());

    log && console.log('refreshed:', format(new Date(), 'HH:mm:ss'));
  } catch (error) {
    log && console.log('### SET ###', error);

    updateStore.update(n => {
      n.success = false;
      return n;
    });

    log && console.log('error fetching update', error);
  }

  // console.log(result)
  return result;
};

/**
 * **********
 * LOCAL
 * **********
 */
const local = async () => {
  // console.log('run forage')

  let result = null;

  try {
    result = await localforage.getItem('settings');

    log && console.log('got storredsettings', await result);
    log && console.log('got storredupdate', await localforage.getItem('refreshed'));
  } catch (error) {
    /* eslint-disable prefer-destructuring */
    log && console.log('failed geting local storage', error);
  }
  // logging && console.log('got api settings', newsettings)

  return result;
};

/**
 * **********
 * EXPORTS
 * **********
 */

// export default update
export { remote, local };
