// @ts-ignore
// import store from '../index'
// @ts-ignore
import { differenceInMinutes } from 'date-fns';
import * as defsettings from '../../config/settings.json';

const { log, online } = defsettings;

const checkIfNeeded = (refreshed, success, updateInterval) => {
  /* eslint-env browser */
  // is online app?
  const condition1 = online;

  // online now?
  const condition2 = navigator.onLine;

  // no updates?
  const condition3 = updateInterval !== 0;

  // not refreshed recently?
  const diff = refreshed instanceof Date ? differenceInMinutes(new Date(), refreshed) : 0;
  const condition4 = refreshed === {} || refreshed === null || diff >= updateInterval;

  // failed recently?
  const condition5 = !success;

  log && console.log('CONDITIONS:');
  log && console.log('settings online:', condition1);
  log && console.log('detect online:', condition2);
  log && console.log('update int !==0:', condition3);
  log && console.log('refreshed diff >= updateInterval:', condition4);
  log && console.log('!success:', condition5);

  if ((condition1 && condition2 && condition3 && condition4) || (condition1 && condition2 && condition3 && condition5)) {
    log && console.log('update needed');
    return true;
  }
  log && (condition2 ? console.log('update NOT needed') : console.log('update NOT possible'));
  return false;
};

export default checkIfNeeded;
