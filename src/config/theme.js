// ***************** */
//  BASE STYLE       */
// ***************** */
const mainColor = '#fff';
const backgroundColor = '#011289'; // choose null if non-transparent background image should be used
const highlightColor = '#ee3300';

const appStyle = `background-color:${backgroundColor};color:${mainColor};fill:${mainColor}`;

// ***************** */
//  OVERRIDE STYLES  */
// ***************** */

//* **  HEADER **/
const headerStyle = 'background: rgba(0, 0, 0, 0.5);';
const logoStyle = 'width:10vh';

//* **  CLOCK **/
const clockStyle = `border-top: 2px solid rgba(255, 255, 255, 1);border-bottom: 2px solid rgba(255, 255, 255, 1);`;

//* **  PRAYERS **/
const prayersStyle = `padding: 0;background:${mainColor};border-top: 0;border-bottom: 0.25vh solid rgba(255, 255, 255, 1);`;
const prayerHeaderStyle = `height: auto;padding: 1.5vh 2vh;margin: 0;font-size:100%;background:${backgroundColor};border-top: .25vh solid rgba(255, 255, 255, 1);border-bottom: .25vh solid rgba(255, 255, 255, 1);`;
const prayerMainStyle = ``;
const prayerRowStyle = `background:${backgroundColor};margin:1px;`;
const prayerNextStyle = `background-color:${mainColor};color: ${highlightColor}`;

//* **  MESSAGE **/
const messageStyle = '';
const messageTextStyle = 'font-size:80%;font-weight:700;';
const messageRefStyle = 'font-size:75%;';

//* **  COUNTDOWN **/
const countdownStyle = `border-top: 2px solid rgba(255, 255, 255, 1);border-bottom: 2px solid rgba(255, 255, 255, 1);`;
const barStyle = 'background: rgba(255, 255, 255, 0.5);';
const barBgStyle = 'background: rgba(255, 255, 255, 0.15);';

//* **  FOOTER **/
const footerStyle = 'background: rgba(0, 0, 0, 0.5);';

export default appStyle;
export {
  appStyle,
  headerStyle,
  logoStyle,
  clockStyle,
  prayersStyle,
  prayerRowStyle,
  prayerHeaderStyle,
  prayerMainStyle,
  prayerNextStyle,
  messageStyle,
  messageTextStyle,
  messageRefStyle,
  countdownStyle,
  barStyle,
  barBgStyle,
  footerStyle,
};
