// ***************** */
//  BASE STYLE       */
// ***************** */
const mainColor = '#333';
const backgroundColor = '#aecfae'; // choose null if non-transparent background image should be used
const highlightColor = '#000';

const appStyle = `background-color:${backgroundColor};color:${mainColor};fill:${mainColor}`;

// ***************** */
//  OVERRIDE STYLES  */
// ***************** */

//* **  HEADER **/
const headerStyle = 'background: none; background-image: linear-gradient(rgba(0,0,0,.5), rgba(0,0,0,.15), rgba(0,0,0,0));color: #f9f9f9;';
const logoStyle = 'fill: #fff;';

//* **  CLOCK **/
const clockStyle = 'align-items: flex-end;margin-top: 1vh;border: none;';

//* **  PRAYERS **/
const prayersStyle = 'border-top:none;';
const prayerRowStyle = ``;
const prayerHeaderStyle = ``;
const prayerMainStyle = ``;
const prayerNextStyle = `color: ${highlightColor}`;

//* **  MESSAGE **/
const messageStyle = '';
const messageTextStyle = 'font-size:80%;font-weight:700;';
const messageRefStyle = 'font-size:75%;';

//* **  COUNTDOWN **/
const countdownStyle = ``;
const barStyle = '';
const barBgStyle = '';

//* **  FOOTER **/
const footerStyle = '';

export default appStyle;
export {
  appStyle,
  headerStyle,
  logoStyle,
  clockStyle,
  prayersStyle,
  prayerRowStyle,
  prayerHeaderStyle,
  prayerMainStyle,
  prayerNextStyle,
  messageStyle,
  messageTextStyle,
  messageRefStyle,
  countdownStyle,
  barStyle,
  barBgStyle,
  footerStyle,
};
